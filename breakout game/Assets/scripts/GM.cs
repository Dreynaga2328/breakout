﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GM : MonoBehaviour
{

    public int lives = 3;
    public int bricks = 20;
    public float resetDelay = 1f;
     public Text timerText;
    public Text livesText;
    public GameObject gameOver;
    public GameObject youWon;
    public GameObject bricksPrefab;
    public GameObject paddle;
    public GameObject deathParticles;
    public static GM instance = null;
    private float Stretchtimer;
   

    float timer;

    private GameObject clonePaddle;

    // Use this for initialization

    private void Start()
    {
        SetTimerText();
    }

    private void Update()
    {

        timer += Time.deltaTime;
        SetTimerText();
        if (timer >= 30f) // sets the timer
        {
            print("Game Over");
        }

        if (Stretchtimer > 0f)
        {
            Stretchtimer -= Time.deltaTime;

            if (Stretchtimer <= 0f)
            {
                clonePaddle.transform.localScale = new Vector3(3f, 1f, 1f);
            }

           
        }
    }



    void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);

        Setup();

    }

    public void Setup()
    {
        clonePaddle = Instantiate(paddle, transform.position, Quaternion.identity) as GameObject;
        Instantiate(bricksPrefab, transform.position, Quaternion.identity);
    }

    void CheckGameOver()
    {
        if (bricks < 1)
        {
            youWon.SetActive(true);
            Time.timeScale = .25f;
            Invoke("Reset", resetDelay);
        }

        if (lives < 1)
        {
            gameOver.SetActive(true);
            Time.timeScale = .25f;
            Invoke("Reset", resetDelay);
        }

    }

    void Reset()
    {
        Time.timeScale = 1f;
        Application.LoadLevel(Application.loadedLevel);
    }

    public void LoseLife()
    {
        lives--;
        livesText.text = "Lives: " + lives;
        Instantiate(deathParticles, clonePaddle.transform.position, Quaternion.identity);
        Destroy(clonePaddle);
        Invoke("SetupPaddle", resetDelay);
        CheckGameOver();
    }

    void SetupPaddle()
    {
        clonePaddle = Instantiate(paddle, transform.position, Quaternion.identity) as GameObject;
    }

    public void DestroyBrick()
    {
        bricks--;
        CheckGameOver();
    }

    public void StretchPaddle()
    {
        clonePaddle.transform.localScale = new Vector3(9f, 1f, 1f);
        Stretchtimer = 15f;


    }

    void SetTimerText()
    {
        timerText.text = "Timer" + timer.ToString();
    }
}