﻿using UnityEngine;





public class ball : MonoBehaviour
{

    public float ballInitialVelocity = 600f;


    private Rigidbody rb;
    private bool ballInPlay;
   

    void Awake()
    {

        rb = GetComponent<Rigidbody>();

    }

    void Update()
    {
        if (Input.GetButtonDown("Fire1") && ballInPlay == false)
        {
            Launch();
        }
    }


    public void Launch()
    {
        transform.parent = null;
        ballInPlay = true;
        rb.isKinematic = false;
        rb.AddForce(new Vector3(ballInitialVelocity, ballInitialVelocity, 0));
    }
   
}
