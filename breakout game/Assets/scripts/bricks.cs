﻿using UnityEngine;
using System.Collections;

public class bricks : MonoBehaviour
{
    public GameObject ballPrefab;
    public GameObject brickParticle;
    public int hits = 2;
    public bool StretchBrick;
    public bool makeBall;

    void OnCollisionEnter(Collision other)
    {
        hits -= 1;
       
        if ( hits == 0)
        {
            Instantiate(brickParticle, transform.position, Quaternion.identity);
            GM.instance.DestroyBrick();
            Destroy(gameObject);

            if (makeBall == true)
            {
            
                GameObject ball = Instantiate(ballPrefab, transform.position, Quaternion.identity) as GameObject;
                ball.GetComponent<ball>().Launch();

            }

            if (StretchBrick == true)
            GM.instance.StretchPaddle();
        }

        
    }
}


